name := "reactivekafka"

organization := "example"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.11.2"

crossScalaVersions := Seq("2.10.4", "2.11.2")

libraryDependencies += "com.softwaremill.reactivekafka" %% "reactive-kafka-core" % "0.8.3"

initialCommands := "import example._"

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.softwaremill.react.kafka.ProducerProperties;
import com.softwaremill.react.kafka.PropertiesBuilder;
import com.softwaremill.react.kafka.ReactiveKafka;
import kafka.serializer.StringEncoder;
import org.reactivestreams.Subscriber;

import java.util.Arrays;

/**
 * Created by Oresztesz_Margaritis on 12/12/2015.
 */
public class Producer {

    public static void main(String[] args) {
        String zooKeeperHost = "192.168.33.10:2181";
        String brokerList = "192.168.33.11:9092";

        ReactiveKafka kafka = new ReactiveKafka();
        ActorSystem system = ActorSystem.create("ReactiveKafka");
        ActorMaterializer materializer = ActorMaterializer.create(system);


        ProducerProperties<String> pp = new PropertiesBuilder.Producer(
                brokerList,
                zooKeeperHost,
                "kafka-stream",
                new StringEncoder(null)).build();

        Subscriber<String> kafkaProducer = kafka.publish(pp, system);


        Source.from(Arrays.asList("One", "Two", "Three"))
                .to(Sink.create(kafkaProducer)).run(materializer);
    }

}

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.softwaremill.react.kafka.ConsumerProperties;
import com.softwaremill.react.kafka.PropertiesBuilder;
import com.softwaremill.react.kafka.ReactiveKafka;
import kafka.message.MessageAndMetadata;
import kafka.serializer.StringDecoder;
import org.reactivestreams.Publisher;

/**
 * Created by Oresztesz_Margaritis on 12/12/2015.
 */
public class Consumer {

    public static void main(String[] args) {
        String zooKeeperHost = "192.168.33.10:2181";
        String brokerList = "192.168.33.11:9092";

        ReactiveKafka kafka = new ReactiveKafka();
        ActorSystem system = ActorSystem.create("ReactiveKafka");
        ActorMaterializer materializer = ActorMaterializer.create(system);

        ConsumerProperties<String> cp = new PropertiesBuilder.Consumer(brokerList, zooKeeperHost, "kafka-stream",
                "groupId", new StringDecoder(null)).build();

        Publisher<MessageAndMetadata<byte[], String>> kafkaConsumer = kafka.consume(cp, system);

        Source.from(kafkaConsumer).map(msg -> msg.message()).map(String::toUpperCase)
                .to(Sink.foreach(System.out::println)).run(materializer);
    }

}
